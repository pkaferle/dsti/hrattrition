# HRattrition

In this project we explored synthetic dataset, simulating employee data from HR database. First, we formatted it and constructed additional variables. Then, we applied different methodologies to find the model that would best describe the relationship between available attributes and employee withdrawal.

Methods:
- Kaplan-Meier estimator
- Cox proportional hazard model
- Elastic-Net for variable selection
- Model performance (log rank ratio, ROC+AUC)




![Capture](/uploads/d397031d7118230158ca487c7714297d/Capture.PNG)